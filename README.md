# PlacesVisited

![Screenshot of the rendered map and description panel](docs/screenshot.png)

This is just a fun POC travel map made with ReactJS and LeafletJS. I made this primarily to try out React, but it's also a fun way to showcase my trips for myself and my family.

Since this is a proof of concept, Babel is still being used in the browser.

## Libraries and languages used:

* Javascript
* ReactJS
* LeafletJS
* JSON