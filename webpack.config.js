const path = require('path');
const outputDir = path.resolve(__dirname, 'js');

module.exports = {    
    mode: 'development',
    entry: path.resolve(__dirname, '/js_src/app.js'),
    output: { 
        path: outputDir,
        filename: 'app.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    }
};