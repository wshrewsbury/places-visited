import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useFetch } from "react-async";

const Container = () => {
  const [selected, setSelected] = useState(false);
  const [locations, setLocations] = useState(false);

  const onLocationClick = (location) => {
    const clickedLocation = locations.find((loc) => {
      return loc.name == location;
    });
    setSelected(clickedLocation);
  };

  const { data } = useFetch("./data.json", {
    headers: { accept: "application/json" },
  });

  useEffect(() => {
    setSelected(data?.location);
    setLocations(data?.locations);
  }, [data]);

  return locations ? (
    <div>
      <MapComponent locations={locations} clickHandler={onLocationClick} />
      <InfoComponent location={selected} />
    </div>
  ) : (
    <div>Loading ...</div>
  );
};

const MarkerList = ({ locations, onClick }) => {
  const iconCamera = L.icon({
    iconUrl: "./pics/camera.png",
    iconSize: [30, 30],
  });

  const markers = locations.map(({ name, position, pics }) => {
    if (Array.isArray(pics)) {
      return (
        <Marker
          icon={iconCamera}
          key={name}
          eventHandlers={{ click: () => onClick(name) }}
          position={position}
        ></Marker>
      );
    } else {
      return (
        <Marker key={name} position={position}>
          <Popup>{name}</Popup>
        </Marker>
      );
    }
  });

  return <React.Fragment>{markers}</React.Fragment>;
};

const MapComponent = ({ locations, clickHandler }) => {
  return (
    <MapContainer center={[51.505, -0.09]} zoom={3}>
      <TileLayer
        attribution="Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012"
        url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}"
      />
      <MarkerList locations={locations} onClick={clickHandler} />
    </MapContainer>
  );
};

const InfoComponent = ({ location }) => {
  let description;
  let pics;

  if (location?.description) {
    description = <p>{location.description}</p>;
  }

  if (location?.pics) {
    pics = location.pics.map(({ url }) => <img key={url} src={url}></img>);

    pics = <div className="pic-container">{pics}</div>;
  }

  return (
    <div className="info-container">
      <h1 style={{ margin: 0, padding: 0 }}>{location?.name}</h1>
      {description}
      {pics}
    </div>
  );
};

// ========================================

ReactDOM.render(<Container />, document.getElementById("root"));
